import time
from DAO.BookDAO import BookDAO


class Books:
    bookdao = BookDAO()

    def __init__(self):
        self.title = None
        self.author = None
        self.price = None
        self.bought_date = None
        self.section = None
        self.status = None

    def add_book(self):
        """ method to add new book details to database
        """
        time_now = time.strftime('%Y-%m-%d')
        read = raw_input("\nenter title,author,price and section(computer, electrical, electronic, magazines, "
                         "novels) of user\n").split()
        self.bookdao.insert_book(read, time_now)

    def search_book(self, book):
        """ method to search a particlar book from database
        """
        self.title = book
        book_status = self.bookdao.search_book(self.title)
        return book_status

    def search_all(self):
        """ method to fetch all books
        """
        books = self.bookdao.search_all()
        print "%12s%12s%12s%12s" % ('title', 'author', 'section', 'status')
        for row in books:
            print "%12s%12s%12s%12s" %(row.get('title'), row.get('author'), row.get('section'), row.get('status'))
