from utility.DB import DB


class BookDAO:
    __db = None

    def __init__(self):
        self.__db = DB()

    def search_book(self,book_name):
        return self.__db.query("SELECT * FROM BOOK WHERE TITLE = '%s' AND STATUS = 'available'" % book_name, None).fetchall()

    def insert_book(self,read,time):
        print read, time
        self.__db.query("INSERT INTO BOOK (TITLE,AUTHOR,PRICE,BOUGHT_DATE,SECTION) VALUES (%s, %s, %s, %s, %s)",
                   (read[0], read[1], read[2], time, read[3]))

    def search_all(self):
        return self.__db.query("SELECT * FROM BOOK", None).fetchall()
