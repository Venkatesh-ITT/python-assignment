from utility.DB import DB


class IssueDAO:
    __db = None

    def __init__(self):
        self.__db = DB()

    def check_issue_count(self, member_id):
        return self.__db.query("SELECT COUNT(MEMBER_ID) FROM ISSURE WHERE MEMBER_ID = '%s' AND "
                               "RETURN_DATE='0000-00-00' " % member_id, None).fetchone()

    def issue_book(self, member_id, staff_id, book_id, date, due_date):
        self.__db.query("INSERT INTO ISSURE(MEMBER_ID,STAFF_ID,BOOK_ID,ISSUE_DATE,DUE_DATE) "
                        "VALUES(%s,%s,%s,%s,%s)", (member_id, staff_id, book_id, date, due_date))
        self.__db.query("UPDATE BOOK SET STATUS = 'BORROWED' WHERE BOOK_ID = '%s'" % book_id, None)

    def search_staff(self, name):
        return self.__db.query("SELECT * FROM LIBRARY_STAFF WHERE  STAFF_NAME = '%s'" % name,
                               None).fetchall()
