from utility.DB import DB


class MemberDAO:
    __db = None

    def __init__(self):
        self.__db = DB()

    def search_member(self, member_name):
        return self.__db.query("SELECT * FROM MEMBERS WHERE MEM_NAME = '%s'" % member_name,
                               None).fetchall()

    def insert_member(self, read):
        self.__db.query("INSERT INTO MEMBERS (MEM_NAME,ADDRESS,TYPE) VALUES (%s, %s, %s)",
                              (read[0], read[1], read[2]))
