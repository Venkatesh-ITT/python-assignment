from LibraryStaff import LibraryStaff


if __name__ == "__main__":
    try:
        staff_object = LibraryStaff()
        read_name = raw_input("enter staff name\n")
        check = staff_object.search_staff(read_name)
        if check is True:
            while True:
                choice = raw_input(
                    '\n1 : add member\n2 : add book\n3 : issue book\n4 : search book\n5 : search member\n6 : '
                    'Find all books\n7 : exit\nenter your choice\n')
                if choice == "1":
                    staff_object.add_member()
                elif choice == "2":
                    staff_object.add_book()
                elif choice == "3":
                    staff_object.issue_book()
                elif choice == "4":
                    book_name = raw_input("enter book name you want to search\n")
                    x = staff_object.search_book(book_name)
                    print "book found"
                    print "  %12s%12s%12s%12s" % ('title', 'author', 'section', 'status')
                    if len(x) > 0:
                        print "  %12s%12s%12s%12s\n" % (x[0].get('title'), x[0].get('author'), x[0].get('section'), x[0].get('status'))
                    else:
                        print "book not available"
                elif choice == "5":
                    member = raw_input("enter member name you want to search\n")
                    y = staff_object.search_member(member)
                    print "member found"
                    print "  %12s%12s" % ('id', 'name')
                    if len(y) > 0:
                        print "  %12s%12s\n" %(y[0].get('member_id'), y[0].get('mem_name'))
                    else:
                        print "member not registered"
                elif choice == "6":
                    staff_object.search_all()
                elif choice == "7":
                    break
                else:
                    print "invalid choice"
        else:
            "invalid staff"
    except Exception as e:
        print e