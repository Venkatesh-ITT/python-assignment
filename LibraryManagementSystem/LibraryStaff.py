import datetime
import time
from DAO.IssueDAO import IssueDAO
from Books import Books
from Members import Members


class LibraryStaff(Books, Members):
    issuedao = IssueDAO()

    def __init__(self):
        Books.__init__(self)
        Members.__init__(self)
        self.staff_id = None
        self.staff_name = None
        self.designation = None
        self.staff_address = None

    def search_staff(self,name):
        """ method to search particular staff from database
        """
        self.staff_name = name
        value = self.issuedao.search_staff(self.staff_name)
        if len(value) > 0:
            pwd = raw_input("enter password\n")
            if pwd == '1234':
                self.staff_id = value[0].get('staff_id')
                self.staff_name = value[0].get('staff_name')
                self.designation = value[0].get('designation')
                self.staff_address = value[0].get('address')
                return True
            else:
                print "invalid password"
                return False
        else:
            print "invalid staff name"
            return False

    def issue_book(self):
        """ method to issue book to a student
        """
        time_now = time.strftime('%Y-%m-%d')
        date_var = datetime.datetime.strptime(time_now, "%Y-%m-%d")
        delay_var = datetime.timedelta(days=15)
        due_date = date_var + delay_var
        read=raw_input("enter book name \n")
        book_status = Books.bookdao.search_book(read)
        if len(book_status) > 0:
            member_name = raw_input("enter member name \n")
            member_status = Members.memberdao.search_member(member_name)
            if len(member_status) > 0:
                count = self.issuedao.check_issue_count(member_status[0].get('member_id'))
                if count.get('COUNT(MEMBER_ID)') >= 3:
                    print "already issued 3 books"
                else:
                    self.issuedao.issue_book(member_status[0].get('member_id'), self.staff_id, book_status[0].get('book_id'), time_now, due_date)
                    print "book issued, due is on ", due_date
            else:
                print "invalid member"
        else:
            print "book not available"
