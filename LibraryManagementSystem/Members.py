from DAO.MemberDAO import MemberDAO


class Members:
    memberdao = MemberDAO()

    def __init__(self):
        self.member_name = None
        self.member_address = None
        self.member_type = None

    def add_member(self):
        """ method to add members to  database
        """
        read = raw_input("\nenter name,address and type(student,teacher) of user\n").split()
        self.memberdao.insert_member(read)

    def search_member(self, name):
        """ method to search particular member from database
        """
        self.member_name = name
        member_status = self.memberdao.search_member(self.member_name)
        return member_status
