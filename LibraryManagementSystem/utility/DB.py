import MySQLdb

class DB(object):
    def __init__(self):
        self.db = MySQLdb.connect(host="localhost", user="root", passwd="1234", db="library")
        self.cursor_object = self.db.cursor(MySQLdb.cursors.DictCursor)

    def query(self, query, params):
        self.cursor_object.execute(query, params)
        self.db.commit()
        return self.cursor_object

    def close(self):
        self.db.close()
