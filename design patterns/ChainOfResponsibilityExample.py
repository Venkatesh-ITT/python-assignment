class Event:
    def __init__(self, name):
        self.name = name

class Widget:
    def __init__(self, parent=None):
        self.__parent = parent

    def Handle(self, event):
        handler = 'Handle_' + event.name
        if hasattr(self, handler):
            method = getattr(self, handler)
            method(event)
        elif self.__parent:
            self.__parent.Handle(event)
        elif hasattr(self, 'HandleDefault'):
            self.HandleDefault(event)


class MainWindow(Widget):
    def Handle_close(self, event):
        print 'MainWindow: ' + event.name

    def HandleDefault(self, event):
        print 'Default: ' + event.name


class SendDialog(Widget):
    def Handle_paint(self, event):
        print 'SendDialog: ' + event.name


class MsgText(Widget):
    def Handle_down(self, event):
        print 'MsgText: ' + event.name


mainwindow_object = MainWindow()
senddialog_object = SendDialog(mainwindow_object)
msg = MsgText(senddialog_object)
edown = Event('down')
msg.Handle(edown)
epaint = Event('paint')
msg.Handle(epaint)
eodd = Event('odd')
msg.Handle(eodd)
