
class Singleton:
    __single = None

    def __init__(self):
        if Singleton.__single:
            raise Singleton.__single
        Singleton.__single = self


class Child(Singleton):
    def __init__(self, name):
        Singleton.__init__(self)
        self.__name = name

    def name(self):
        return self.__name

class Junior(Singleton):
    pass


def Handle(x=Singleton):
    try:
        single = x()
    except Singleton, s:
        single = s
    print single
    return single


child = Child('Monty')
junior = Handle(Junior)
print junior.name()
single = Handle()
print single.name()
name = 'Junior'
sclass = globals()[name]
junior = Handle(sclass)
print junior.name()

