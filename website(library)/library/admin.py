from django.contrib import admin
from .models import staff, Book, Member, issue

admin.site.register(staff)
admin.site.register(Member)
admin.site.register(Book)
admin.site.register(issue)

