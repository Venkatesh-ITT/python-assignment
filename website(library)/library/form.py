from django import forms
from library.models import issue, staff


class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = staff
        fields = ['email', 'password']


class IssueForm(forms.ModelForm):
    class Meta:
        model = issue
        fields = ['member_id', 'book_id']