from django.db import models
from django import forms
import time
import datetime
from django.contrib.auth.models import User


# Create your models here.

class Member(models.Model):
    MEMBER_TYPE = (
        ('STUDENT', 'STUDENT'),
        ('TEACHER', 'TEACHER'),
    )
    name = models.CharField(max_length=50)
    email = models.EmailField()
    address = models.CharField(max_length=80)
    type = models.CharField(max_length=10, choices=MEMBER_TYPE)

    def __str__(self):
        return "%s: %s" % (self.id, self.name)


class Book(models.Model):
    BOOK_TYPE = (
        ('Computer', 'Computer'),
        ('Electrical', 'Electrical'),
        ('Electronic', 'Electronic'),
        ('Magazines', 'Magazines'),
        ('Novels', 'Novels')
    )
    now = time.strftime('%Y-%m-%d')
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    price = models.IntegerField()
    bought_date = models.DateField(default=now, editable=False)
    section = models.CharField(max_length=15, choices=BOOK_TYPE)
    quantity = models.IntegerField()

    def __str__(self):
        return "%s-%s-%s-%s" % (self.id, self.title, self.author, self.quantity)


class staff(models.Model):
    DESIGNATION_TYPE = (
        ('MANAGER', 'MANAGER'),
        ('LIBRARIAN', 'LIBRARIAN')
    )
    name = models.CharField(max_length=50)
    designation = models.CharField(max_length=15, choices=DESIGNATION_TYPE)
    email = models.EmailField()
    address = models.CharField(max_length=80)
    password = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class issue(models.Model):
    date_now = time.strftime('%Y-%m-%d')
    date_var = datetime.datetime.strptime(date_now, "%Y-%m-%d")
    delay_var = datetime.timedelta(days=15)
    due_date = date_var + delay_var
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE)
    staff_id = models.ForeignKey(staff, on_delete=models.CASCADE)
    member_id = models.ForeignKey(Member, on_delete=models.CASCADE)
    issue_date = models.DateField(default=date_now, editable=False)
    due_date = models.DateField(default=due_date, editable=False)
    returned_date = models.DateField(default='1111-11-11')

    def __str__(self):
        return "%s-%s-%s-%s" % (self.member_id, self.staff_id, self.due_date, self.returned_date)


class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = staff
        fields = ['email', 'password']


class IssueForm(forms.ModelForm):
    class Meta:
        model = issue
        fields = ['member_id', 'book_id']
