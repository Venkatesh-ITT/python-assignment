from django.shortcuts import render


# Create your views here.
def index(request):
    """Renders the home page."""
    return render(
        request,
        'index.html',
    )


def home(request):
    staff_name = request.session['username']
    return render(request, 'home.html', {'username': staff_name, 'message': "Welcome to Library Management System"})
