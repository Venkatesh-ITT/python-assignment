from django.shortcuts import render
from library.models import Book, issue
from library.form import IssueForm


def issue_book(request):
    staff_id = request.session['staff_id']
    staff_name = request.session['username']
    if request.method == "GET":
        form = IssueForm()
        return render(request, 'issue.html', {'form': form, 'username': staff_name, 'message': "Issue Books"})
    elif request.method == "POST":
        book_id = request.POST['book_id']
        member_id = request.POST['member_id']
        book_count = issue.objects.filter(member_id=member_id, returned_date='1111-11-11').count()
        issue_count = issue.objects.filter(member_id=member_id, returned_date='1111-11-11', book_id=book_id).count()
        book_instance = Book.objects.get(id=book_id)
        if issue_count == 0:
            if book_instance.quantity > 0:
                if book_count < 3:
                    issues = issue(book_id_id=book_id, staff_id_id=staff_id, member_id_id=member_id)
                    book_instance.quantity = book_instance.quantity - 1
                    book_instance.save()
                    issues.save()
                    return render(request, 'home.html', {'username': staff_name, 'message': "The book has been issued" })
                else:
                    return render(request, 'home.html', {'username': staff_name, 'message': 'Member already borrowed 3 books'})
            else:
                return render(request, 'home.html', {'username': staff_name, 'message': 'Book is not available'})
        else:
            return render(request, 'home.html', {'username': staff_name, 'message': 'this book has already issued once'})