from library.form import LoginForm
from library.models import staff
from django.shortcuts import render


def login(request):
    if request.method == "GET":
        form = LoginForm()
        return render(request, 'login.html', {'form': form})
    elif request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        if staff.objects.filter(email=email) and staff.objects.filter(password=password):
            result = staff.objects.get(email=email)
            request.session['username'] = result.name
            request.session['staff_id'] = result.id
            return render(request, 'home.html', {'username': result.name, 'message': 'Welcome to Library Management System'})
        else:
            return render(request, 'error.html', {'message': "Invalid username or password"})