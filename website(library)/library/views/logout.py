from django.shortcuts import render
from django.http import HttpResponseRedirect


def session(request):
    staff_name = request.session['username']
    try:
        del request.session['username']
        del request.session['staff_id']
        return HttpResponseRedirect('/logout')
    except Exception as e:
        return render(request, 'home.html', {'username': staff_name, 'message': e})


def logout(request):
    return render(request, 'logout.html')