from django.shortcuts import render
from library.models import Book, issue
from library.form import IssueForm
import time


def receive(request):
    staff_id = request.session['staff_id']
    staff_name = request.session['username']
    if request.method == "GET":
        form = IssueForm()
        return render(request, 'issue.html', {'form': form, 'username': staff_name, 'message': "Recieve Book"})
    elif request.method == "POST":
        date_now = time.strftime('%Y-%m-%d')
        book_id = request.POST['book_id']
        member_id = request.POST['member_id']
        check = issue.objects.filter(member_id_id=member_id, book_id_id=book_id, staff_id_id=staff_id, returned_date='1111-11-11').count()
        if check == 1:
            get_details = issue.objects.get(member_id_id=member_id, book_id_id=book_id, staff_id_id=staff_id, returned_date='1111-11-11')
            get_details.returned_date = date_now
            book_check = Book.objects.get(id=book_id)
            book_check.quantity = book_check.quantity + 1
            get_details.save()
            book_check.save()
            return render(request, 'home.html', {'username': staff_name, 'message': 'book recieved'})
        else:
            return render(request, 'home.html', {'username': staff_name, 'message': 'Member didn\'t borrowed that book '})
