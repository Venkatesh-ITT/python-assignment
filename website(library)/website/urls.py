"""
Definition of urls for website.
"""

from django.conf.urls import url
from library.views.home import home, index
from library.views.logout import logout, session
from library.views.login import login
from library.views.issue import issue_book
from library.views.receive import receive
from django.contrib import admin
from django.contrib.auth import views as auth_views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^home/$', home, name='home'),
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^logout1/$', auth_views.logout, name='logout1'),
    url(r'^home/issue/$', issue_book, name='issue'),
    url(r'^home/recieve/$', receive, name='recieve'),
    url(r'^sessionnull/$', session, name='sessionnull')]
