#Write a program to find common values from two list

def common_values(list1,list2):
    '''
    this function will give the common values between list1 and list2
    '''
    common_list=[]
    for item1 in list1:
        if item1 in list2:
            common_list.append(item1)                    
    return common_list

list1=[1,2,3,4,5,9]
list2=[2,4,9,6,8,10]
print "List1 :",list1
print "List2 :",list2
print "common values in list1 and list2 are :",common_values(list1,list2)
