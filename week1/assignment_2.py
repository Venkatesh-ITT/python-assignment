#Write a program to remove duplicate elements from given list with original order.

def remove_duplicate(list1):
    '''
    this function will remove duplicate elements from give list
    '''
    list_without_duplicate=[]
    list_removed_elements=[]
    for item in list1:
        if item not in list2:
            list_without_duplicate.append(item)
        elif item in list2:
            list_removed_elements.append(item)
    print "List after removing duplicates is :",list_without_duplicate
    print "List of removed items are :",list_removed_elements
    

list1=[1,1,1,1,3,3,3,4,4,4,4,5,5,5,55,6]
print "Initial List was :",list1
remove_duplicate(list1)
