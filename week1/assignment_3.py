#Write a program to sort list, dictionary. 

def bubble_sort(data):
    '''
    this function will return the sorted list of data variable, it can be dictionary or list
    '''
    if type(data)==dict:
        items = data.keys()
        
    elif type(data)==list:
        items=data
    else:
        print "invalid input!! enter either list or dictionary"
    
    for j in range(len(items) - 1):
        for i in range(len(items) - 1):
            items[i], items[i+1] = order(items[i], items[i+1])
    return items

def order(item1, item2):
    '''
    this function returned the item1 and item2 in decending order
    '''
    if item1 < item2:
        return item1, item2
    else:
        return item2, item1


mydict = {1:'a', 4:'b', 9:'c', 3:'d', 8:'e'}
lists = [7,6,5,4,3,2,1]
new_dictionary=bubble_sort(mydict)
new_list=bubble_sort(lists)

print "sorted list :", new_list
print "sorted dictionary : {",
for key in new_dictionary:
    print "%s: '%s'" % (key, mydict[key]),
print "}"
