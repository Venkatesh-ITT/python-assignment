#Write a program to convert decimal number into octal and vice versa.

choice=raw_input("enter your choice \n1->convert decimal to octal,\n2->convert octal to decimal\n")
try:
    if choice=='1':
        decimal = int(input("Enter a decimal integer greater than 0: "))
        if decimal==0:
            print "The octal representation is ",0
        elif decimal>0:
            oct_num = []
            while decimal > 0:
                remainder = decimal % 8
                oct_num.extend([remainder])
                decimal = decimal/8
            print "The octal representation is ",
            for item in reversed(oct_num):
                print item,
        else:
            print "enter positive number"
    
    elif choice=='2':
        count=0
        decimal_no = 0
        octal_num = int(raw_input("Enter a positive octal number greater than 0: "),8)
        if octal_num == 0:
            print "The decimal representation is ",0
        elif octal_num>0:
            while octal_num>0:
                decimal_no=decimal_no+(octal_num%10)*(8**count)
                count +=1
                octal_num = octal_num/10
            print("The decimal representation is "+str(decimal_no))
        else:
            print "enter positive octal number"
    else:
        print "invalid choice!! Try again"
except:
    print "the input is invalid, Try again"
    
