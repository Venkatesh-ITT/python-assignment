# printing prime numbers from 0 to 10


def primenumbers():
    '''
    this function will return prime numbers from 0 to 10
    '''
    prime_list = []
    flag = 1
    for number1 in range(2, 11):
        for number2 in range(2, number1):
            if number1 % number2 == 0:
                flag = 0
                break
            else:
                flag = 1
        if flag == 1:
            prime_list.append(number1)
    return prime_list


print "prime numbers are : ", primenumbers()
