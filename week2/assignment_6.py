# Create a class of Bike and Engine and map to mimic a real world bike object.


from time import time


class Engine:
    """
    Description:
    Engine variables and its working
    """
    def __init__(self):
        self.bike_gear = 0
        self.bike_speed = 0

    def running(self):
        if self.bike_gear == 0:
            self.bike_speed = 0
        elif self.bike_gear == 1:
            self.bike_speed=20
        elif self.bike_gear == 2:
            self.bike_speed = 40
        elif self.bike_gear == 3:
            self.bike_speed = 60
        elif self.bike_gear == 4:
            self.bike_speed = 80


class Bike(Engine):
    """
    Description:
    bike and engine mimic by inheriting the engine in bike
    """

    def __init__(self):
        Engine.__init__(self)
        self.bike_distance = 0

    def bike_start(self):
        while True:
            start_time = time()
            choice1 = raw_input("\npress 'U' to gear-up and 'D' to gear-down and press 'S' to stop the bike\n")
            end_time = time()
            self.bike_distance = self.bike_distance + (((end_time-start_time)/60)/60)*self.bike_speed
            if choice1.upper() == "U" and self.bike_gear < 4:
                self.bike_gear += 1
                Engine.running(self)
                print "gear %d, speed %d km/h, distance covered %f KM." % (self.bike_gear,self.bike_speed,self.bike_distance)
            elif choice1.upper() == "D" and self.bike_gear>0:
                self.bike_gear -=1
                Engine.running(self)
                print "gear %d, speed %d km/h, distance covered %f KM." % (self.bike_gear, self.bike_speed, self.bike_distance)
            elif choice1.upper() == "S":
                return self.bike_distance
            else:
                print "gear %d, speed %d km/h, distance covered %f KM." % (self.bike_gear, self.bike_speed, self.bike_distance)
                print "maximum gear is 4 and minimum is 0(idle) shift accordingly"


class User(Bike):
    """
    Description:
    Getting the user details and making user use the bike
    """

    def __init__(self):
        Bike.__init__(self)
        self.user_name = None
        self.user_age = 0
        self.user_gender = 0

    def getuser(self, name1, age1, gender1):
        self.user_name = name1
        self.user_age = age1
        self.user_gender = gender1
        print "%s has started the bike " % self.user_name
        Bike.bike_start(self)
        print "%s has travelled %f kms" % (self.user_name,self.bike_distance)

if __name__ == "__main__":
    try:
        while True:
            print ""
            choice = raw_input('1:start Bike\n2:exit\nenter your choice\n\n')
            if choice == "1":
                user_object = User()
                name, age, gender = map(str,raw_input("enter name, age and gender (ex: xyz 25 male)  : ").strip().split(" "))
                if 18 < int(age) < 115 and len(name) > 2:
                    if gender.lower() == "male" or gender.lower() == "female":
                        user_object.getuser(name, int(age), gender)
                    else:
                        print "invalid gender!! try again"
                        continue
                else:
                    print "invalid name or age!! try again"
                    continue
            elif choice == "2":
                break
            else:
                print "invalid choice!! Try again"

    except Exception as ex:
        print ex
