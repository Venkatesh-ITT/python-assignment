#  to read XML file and convert into JSON file.

import re
# regular expression

file1 = open("xml_file.xml", "r")
file2 = open("json_created.json","w")
item = []

while True:
    line_file1 = file1.readline()
    pattern = re.sub("[<>/""]", " ", line_file1)
    string = re.split('(\W+)', pattern)
    string = ''.join(string).split()
    item = item+string
    if not line_file1:
        break
tag = item[1]
line = "{ \""+item[0]+"\" :[\n"
file2.write(line)
count = 0
for element in range(len(item)):
    if element == 0 or element == len(item)-1:
        continue
    elif item[element] == tag and element < 2:
        file2.write("         { \"",)
        continue
    elif item[element] != item[element-1] and item[element] == tag:
        file2.write("\" },\n     ")
        continue
    elif item[element] == item[element-1] or item[element] == tag:
        file2.write("    { \"",)
        continue
    elif item[element] == item[element-2] and count < 1:
        file2.write("\" , \"",)
        count=count+1
        continue
    elif item[element] == item[element-2] and count == 1:
        count=count-1
        continue
    elif item[element] == item[element+2] and element < len(item)-2:
        line=item[element]+"\" : \""
        file2.write(line)
    else:
        file2.write(item[element])
file2.write("\n] }")
file1.close()
file2.close()
