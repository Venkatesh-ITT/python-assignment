''' Write a Class to communicate with Facebook Graph API using python request module to perform following action:
a. create post
b. get friends
c. get profile
d. update profile
e. delete post '''

import facebook


class FacebookCommunicate(object):
    '''
    this class will fetch a profile details and friends list from facebook using access token
    '''
    #below token will expire every one hour. so need to generate and place token again
    token = 'EAACEdEose0cBALZB5khlqAVxseAZAytsDl6agkJGZALy6GRdErofkqUVb8mm9nk3NtDrvsOZCWK6jrZAGNZBjpceIcMZAYvauajlcxR3qKLJUXHtI9dKZAx5vkvxm1tJdpC81AnjlmBzpOrcVGAmWX0Bk0AiLuLfRvxOJMYVz8C3kdn57pUHVqFZCOhqESMEPQvoZD'
    graph = facebook.GraphAPI(token)
    args = {'fields': 'id,name,email,birthday,gender,hometown,religion', }
    friend_list = []
    def fetch(self):
        self.profile = FacebookCommunicate.graph.get_object('me', **FacebookCommunicate.args)
        self.friends = FacebookCommunicate.graph.get_connections("me", "friends")
        for friend in self.friends['data']:
            FacebookCommunicate.friend_list.append(friend['name'])
    def display(self):
        print "User Name: ", self.profile['name']
        print "Email id: ", self.profile['email']
        print "Email id: ", self.profile['birthday']
        print "Gender: ", self.profile['gender']
        print "Hometown: ", self.profile['hometown']
        print "Religion: ", self.profile['religion']
        print "friends list: ", " ,".join(FacebookCommunicate.friend_list)
    def posting(self,msg):
        FacebookCommunicate.graph.put_object('me', "feed", message=msg)
    def delete_post(self,ids):
        FacebookCommunicate.graph.delete_object(id=ids)


if __name__ == "__main__":
    facebook_object = FacebookCommunicate()
    facebook_object.fetch()
    facebook_object.display()
    #f.posting("good afternoon")
    #f.delete_post('1204657739618113_1380822425334976')
