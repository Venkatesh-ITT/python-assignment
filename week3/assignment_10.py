# Fibonacci series using generators


def fibonacciseries():
    '''
    generator which yiels fibonacy series
    '''
    number1, number2 = 0, 1
    while True:
        yield number1
        number1, number2 = number2, number1 + number2


iter_object = fibonacciseries()
count = 0
try:
    limit = int(input("enter the limit to print series : "))
    if limit > 0:
        for number in iter_object:
            print number
            count = count + 1
            if count == limit:
                break
    elif limit == 0:
        pass
    else:
        print "input only positive numbers"
except:
    print "Thats not a number, Try again"
