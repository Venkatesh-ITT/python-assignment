# program that takes a list and iterates it from the reverse direction. (Write iterator class for this)


class ReverseList:
    '''
    iterator class which return reverse of the list
    '''
    def __init__(self, lists):
        self.lists = lists
        self.index = len(self.lists)

    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.lists[self.index]

    def __iter__(self):
        return self

_list = [1, 2, 3, 4, 5, 6, 7]
reverselist_object = ReverseList(_list)

for item in reverselist_object:
    print item,
