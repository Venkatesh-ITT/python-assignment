# take list as an input and gives output as list of tuples. where each tuple will contain (index, value).


def indexvalue(lists):
    '''
    generator that yields a (index, value) combination
    '''
    for item in range(len(lists)):
        yield (item, lists[item])


list1 = [2, 3, 4, 5, 6]
function_object = indexvalue(list1)
new_list = []
for item in function_object:
    new_list.append(item)

print new_list
