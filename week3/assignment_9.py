from bs4 import BeautifulSoup
import urllib2
import re
import os


class tripadvisor:
    def __init__(self):
        '''
        initializing the header and requesting to fetch the content
        '''
        self.headers = {'User-Agent': 'Chrome 41.0.2272.76'}
        self.request = urllib2.Request('https://www.tripadvisor.in/', None, self.headers)
        self.response = ""
        self.url1 = ""

    def get(self):
        '''
        fetching the content from mentioned url
        '''
        self.response = urllib2.urlopen(self.request)
        self.content = self.response.read()
        self.soup = BeautifulSoup(self.content, "lxml")

    def search(self,name):
        '''
        searching the city name in tripadvisor website
        '''        
        for link in self.soup.find_all(href=re.compile(name), class_="ui_link"):
            self.url1 = "https://www.tripadvisor.in" + link.get('href')

        self.response1 = urllib2.urlopen(self.url1)
        self.content1 = self.response1.read()
        self.soup = BeautifulSoup(self.content1, 'html5lib')        #install the html5lib library 
        self.link1 = self.soup.findAll('img', {'class': "photo_image"})
        self.link2 = self.soup.findAll('span', {'class': "viewAll"})
        self.all_script = self.soup.find("script", text=re.compile("var newlazyImgs"))
        if len(str(self.all_script)) > 20:
            self.file_object = open("scriptfile.txt","w")
            self.file_object.writelines(self.all_script)
            self.file_object.close()
            for i in range(min(len(self.link1),len(self.link2))-1):
                prize = self.link2[i].text.split()
                print "hotel Name : ", self.link1[i].get('alt')
                print "Amount     : ", prize[len(prize) - 1]
                with open("scriptfile.txt") as new_file_object:
                   for line in new_file_object:
                        if self.link1[i].get('id') in line:
                            line1=re.sub("[,{}""]"," ", line)
                            photo_link = re.split('(\W+)', line1)
                            photo_link = ''.join(photo_link).split()
                            print "photo      :",photo_link[0][8:len(photo_link[0])-1]
                            break
                print ""
                new_file_object.close()
            os.remove("scriptfile.txt")
        else:
            print "error occurs, try again!!"

if __name__ == "__main__":
    trip_object=tripadvisor()
    trip_object.get()
    print "city names are : "
    names = "Ahmedabad, Alappuzha, Aurangabad, Bengaluru, Bhopal, Bhubaneswar, Chandigarh, Chennai, Coimbatore, Gurugram, Guwahati, Himachal, Hyderabad, Indore, Karnataka, Kerala, Kochi, Kolkata, Kumarakom, Lucknow, Ludhiana, Maharashtra, Mumbai, Nagpur, Nashik, Delhi, Noida, Panjim, Pune, Rajasthan, Sawai, Surat, Tamil_Nadu, Trivandrum, Vadodara, Visakhapatnam, Coorg, Darjeeling, Dehradun, Dharamsala, Gangtok, Kalpetta, Kodaikanal, Lonavala, Mahabaleshwar, Manali, Matheran, Mount_Abu, Munnar, Mussoorie, Nainital, \nOoty, Shillong, Shimla, Srinagar, Thekkady, Yercaud, Amritsar, Haridwar, Katra, Leh, Madurai, Puri, Rishikesh, Shirdi, Tirupati, Varanasi, Calangute, Candolim, Diu, Goa, Kanyakumari, Kovalam, Mangalore, Pondicherry, Port_Blair, Agra, Bikaner, Jaipur, Jaisalmer, Jodhpur, Khajuraho, Mysuru, Pushkar, Udaipur, \n\n"
    print names
    city = raw_input("enter the city name to search for the hotels : ")
    city_list = names.split(", ")
    if city in city_list:
        trip_object.search(city)
    else:
        print "invalid city name"
